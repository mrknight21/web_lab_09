var customers = [
	{ "name" : "Peter Jackson", "gender" : "male", "year_born" : 1961, "joined" : "1997", "num_hires" : 17000 },
		
	{ "name" : "Jane Campion", "gender" : "female", "year_born" : 1954, "joined" : "1980", "num_hires" : 30000 },
	
	{ "name" : "Roger Donaldson", "gender" : "male", "year_born" : 1945, "joined" : "1980", "num_hires" : 12000 },
	
	{ "name" : "Temuera Morrison", "gender" : "male", "year_born" : 1960, "joined" : "1995", "num_hires" : 15500 },
	
	{ "name" : "Russell Crowe", "gender" : "male", "year_born" : 1964, "joined" : "1990", "num_hires" : 10000 },
	
	{ "name" : "Lucy Lawless", "gender" : "female", "year_born" : 1968, "joined" : "1995", "num_hires" : 5000 },	
		
	{ "name" : "Michael Hurst", "gender" : "male", "year_born" : 1957, "joined" : "2000", "num_hires" : 15000 },
		
	{ "name" : "Andrew Niccol", "gender" : "male", "year_born" : 1964, "joined" : "1997", "num_hires" : 3500 },	
	
	{ "name" : "Kiri Te Kanawa", "gender" : "female", "year_born" : 1944, "joined" : "1997", "num_hires" : 500 },	
	
	{ "name" : "Lorde", "gender" : "female", "year_born" : 1996, "joined" : "2010", "num_hires" : 1000 },	
	
	{ "name" : "Scribe", "gender" : "male", "year_born" : 1979, "joined" : "2000", "num_hires" : 5000 },

	{ "name" : "Kimbra", "gender" : "female", "year_born" : 1990, "joined" : "2005", "num_hires" : 7000 },
	
	{ "name" : "Neil Finn", "gender" : "male", "year_born" : 1958, "joined" : "1985", "num_hires" : 6000 },	
	
	{ "name" : "Anika Moa", "gender" : "female", "year_born" : 1980, "joined" : "2000", "num_hires" : 700 },
	
	{ "name" : "Bic Runga", "gender" : "female", "year_born" : 1976, "joined" : "1995", "num_hires" : 5000 },
	
	{ "name" : "Ernest Rutherford", "gender" : "male", "year_born" : 1871, "joined" : "1930", "num_hires" : 4200 },
	
	{ "name" : "Kate Sheppard", "gender" : "female", "year_born" : 1847, "joined" : "1930", "num_hires" : 1000 },
	
	{ "name" : "Apirana Turupa Ngata", "gender" : "male", "year_born" : 1874, "joined" : "1920", "num_hires" : 3500 },
	
	{ "name" : "Edmund Hillary", "gender" : "male", "year_born" : 1919, "joined" : "1955", "num_hires" : 10000 },
	
	{ "name" : "Katherine Mansfield", "gender" : "female", "year_born" : 1888, "joined" : "1920", "num_hires" : 2000 },
	
	{ "name" : "Margaret Mahy", "gender" : "female", "year_born" : 1936, "joined" : "1985", "num_hires" : 5000 },
	
	{ "name" : "John Key", "gender" : "male", "year_born" : 1961, "joined" : "1990", "num_hires" : 20000 },
	
	{ "name" : "Sonny Bill Williams", "gender" : "male", "year_born" : 1985, "joined" : "1995", "num_hires" : 15000 },
	
	{ "name" : "Dan Carter", "gender" : "male", "year_born" : 1982, "joined" : "1990", "num_hires" : 20000 },
	
	{ "name" : "Bernice Mene", "gender" : "female", "year_born" : 1975, "joined" : "1990", "num_hires" : 30000 }	
];

var basic = document.getElementById("basicTable");
//var jSon = JSON.parse(customers);
for (var i =0; i<customers.length; i++)
{
	/*var row = document.createElement("tr");
	var name = document.createElement("td");
    name.appendChild(document.createTextNode(customers.name));
	console.log(row);
    console.log(name);
    row.appendChild(name)
    /*var gender = document.createElement("td");
    gender.appendChild(document.createTextNode(jSon[i].gender));
    row.appendChild(gender);
    var year = document.createElement("td");
    year.appendChild(document.createTextNode(jSon[i].year_born));
    row.appendChild(year);
    var joined = document.createElement("td");
    joined.appendChild(document.createTextNode(jSon[i].joined));
    row.appendChild(joined);
    var number = document.createElement("td");
    number.appendChild(document.createTextNode(jSon[i].num_hires));
    row.appendChild(number);
	basic[0].appendChild(row);*/

    var row = basic.insertRow(-1);
    var namer = row.insertCell(-1);
    console.log(namer);
    var gender = row.insertCell(-1);
    var year = row.insertCell(-1);
    var joined = row.insertCell(-1);
    var number = row.insertCell(-1);
    namer.innerHTML = customers[i].name;
    gender.innerHTML = customers[i].gender;
    year.innerHTML= customers[i].year_born;
    joined.innerHTML=customers[i].joined;
    number.innerHTML=customers[i].num_hires;
}

var genderStat = document.getElementById("genderTable");
var male =0;
var female =0;
for (var a =0; a <customers.length; a ++)
{
 switch (customers[a].gender)
 {
	 case "male": male++;
	 break;
	 case "female": female++;
	 break;
 }
}
var rowG = genderStat.insertRow(-1);
var maleC = rowG.insertCell(-1);
var femaleC = rowG.insertCell(-1);
maleC.innerHTML = male;
femaleC.innerHTML = female;


var AgeStat = document.getElementById("ageTable");
var young =0;
var mid =0;
var old =0;
for (var b =0; b <customers.length; b ++)
{
	var age = 2017 - customers[b].year_born;
    if(age<=30){ young++;}
    if(age>=31 && age <65){mid++;}
    if(age>=65){old++;}
}
var rowA = AgeStat.insertRow(-1);
var youngC = rowA.insertCell(-1);
var midC = rowA.insertCell(-1);
var oldC = rowA.insertCell(-1);
youngC.innerHTML = young;
midC.innerHTML = mid;
oldC.innerHTML = old;


var LoyStat = document.getElementById("loyaltyTable");
var gold =0;
var silver =0;
var bronze =0;
for (var c =0; c <customers.length; c ++)
{
    var years = (2017 - customers[c].joined);
    var weekly = customers[c].num_hires/(years*52);
    if(weekly>4){ gold++;}
    if(weekly>1 && weekly<=4){silver++;}
    if(weekly<1){bronze++;}
}
var rowL = LoyStat.insertRow(-1);
var goldC = rowL.insertCell(-1);
var silverC = rowL.insertCell(-1);
var bronzeC = rowL.insertCell(-1);
goldC.innerHTML = young;
silverC.innerHTML = mid;
bronzeC.innerHTML = old;
document.getElementById("container").style.textAlign= "center";