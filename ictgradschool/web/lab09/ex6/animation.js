/**
 * Created by mche618 on 6/04/2017.
 */


var pages = document.getElementsByClassName("page");
for (var i =0; i < pages.length-1; i ++)
{
    pages[i].style.animationName = "none";
    pages[i].style.animationDelay = "0s";
    pages[i].onclick = flipPageFunction(i);
    console.log("page set click for " + pages[i]+pages[i].style.animationDelay);
}


function flipPageFunction( i) {
    return function(){
        flipPage(i);
    }
}
function flipPage(i)
{
    console.log("function falling for " + pages[i]);
    pages[i].style.animationName = "flippage";
    setTimeout(nextPageWrapper(i), 1500);
}
function nextPageWrapper(i) {
    return function () {
        nextPage(i);
    }
}

function nextPage(i) {
    var nextPage = pages[i+1];
    nextPage.style.zIndex = "1";
}